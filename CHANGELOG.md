# Change Log


## [1.0.7] - 2016-11-10
### Added
- PaymentLinks.java class, which contains the payment links for an order.
- Order uuid in order creation response with 'id' as field.
- Runtime dependency for unirest.

## [1.0.6] - 2016-08-24
### Added
- API implementation for Customer create, update, and get.
- API implementation for Wallet list and refresh.

### Changed
- API implementation for Order create, update, list, status and refund.
- API implementation for Transaction create.
- API implementation for Card create, list and delete.